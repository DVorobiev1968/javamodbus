package ru.DVorobiev;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class ThreadSlave implements Runnable {
    public static final int INIT_THREAD = 0;
    public static final int START_THREAD = 1;
    public static final int ERROR_THREAD = -1;
    public static final int RUN_THREAD = 2;
    public static final int CANCEL_THREAD = 3;

    private final String nameThread;
    private String errMessage;
    private int idNodeSlave;
    private int stateThread;

    Thread threadTest;

    ThreadSlave(String name) {
        stateThread = INIT_THREAD;
        nameThread = name;
    }

    @Override
    public void run() {
        try {
            this.stateThread = RUN_THREAD;
            long start = System.currentTimeMillis();
            errMessage = String.format("Thread %s for Slave Node: %d running...", nameThread,this.idNodeSlave);
            log.info(errMessage);
            SlaveTest slaveTest=new SlaveTest();
            slaveTest.runSlaveTest(this.idNodeSlave);
            long time = System.currentTimeMillis() - start;
            float ms = (float) (time / 1000);
            errMessage =
                    String.format(
                            "Thread %s:Test SlaveTest it`s completed, time: %4.3f(sec.)",
                            nameThread, ms);
            log.info(errMessage);
            Thread.sleep(100);
            this.stateThread = CANCEL_THREAD;
        } catch (InterruptedException e) {
            errMessage = "Error:" + e.getMessage();
            log.error(errMessage);
            this.stateThread = ERROR_THREAD;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        errMessage = String.format("Thread %s started for Slave Node: %d", nameThread, idNodeSlave);
        if (threadTest == null) {
            this.stateThread = START_THREAD;
            threadTest = new Thread(this, nameThread);
            errMessage = String.format("Thread %s new started for Slave Node %d", nameThread, idNodeSlave);
            threadTest.start();
        }
        log.info(errMessage);
    }

    public String getName() {
        return threadTest.getName();
    }

    public void setPriority(int i) {
        threadTest.setPriority(i);
    }

    public int getPriority() {
        return threadTest.getPriority();
    }
}
