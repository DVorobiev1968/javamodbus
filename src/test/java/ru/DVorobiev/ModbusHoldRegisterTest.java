package ru.DVorobiev;

import jlibmodbus.Modbus;
import jlibmodbus.data.ModbusHoldingRegisters;
import jlibmodbus.exception.IllegalDataAddressException;
import jlibmodbus.exception.IllegalDataValueException;
import jlibmodbus.exception.ModbusIOException;
import jlibmodbus.exception.ModbusProtocolException;
import jlibmodbus.master.ModbusMaster;
import jlibmodbus.master.ModbusMasterFactory;
import jlibmodbus.msg.request.ReadHoldingRegistersRequest;
import jlibmodbus.msg.response.ReadHoldingRegistersResponse;
import jlibmodbus.tcp.TcpParameters;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.net.InetAddress;

@Slf4j
public class ModbusHoldRegisterTest {
    private String setHolder32(int Hi, int Low, int sizeof) throws IllegalDataValueException, IllegalDataAddressException {
        int offset=0;
        String errMessage=new String();
        ModbusHoldingRegisters registers =new ModbusHoldingRegisters((int)sizeof/16);
        registers.setImpl(offset++,Hi);
        registers.setImpl(offset,Low);
        float tempValue= registers.getFloat32At(0);
        offset=0;
        for (int r : registers){
            errMessage+=String.format("Addr:%d, Value: %d\n",offset++,r);
        }
        errMessage+=String.format("Float32: %4.15f",tempValue);
        log.debug(errMessage);
        errMessage=String.format("convertByteToFloat: %s",convertByteToFloat(Hi,Low));
        return errMessage;
    }
    private float convertArrayToFloat(int [] registers){
        int offset=0;
        float fValue= 0.0F;
        fValue+=(byte) registers[0];
        fValue+=(byte) (registers[1] >>8);
        return fValue;
    }
    private String convertByteToFloat(int Hi, int Low) throws IllegalDataValueException, IllegalDataAddressException {
        String errMessage=new String();
        int offset=0;
        int [] registers=new int[2];
        registers[offset++]=Hi;
        registers[offset]=Low;

        Float tempValue=convertArrayToFloat(registers);

        errMessage+=String.format("Float32: %4.15f",tempValue);
        log.debug(errMessage);
        errMessage=String.format("Float32: %4.15f",tempValue);
        return errMessage;
    }

    private String setHolder64(int Hi, int Low, int HiHi, int LowLow, int sizeof) throws IllegalDataValueException, IllegalDataAddressException {
        int offset=0;
        String errMessage=new String();
        ModbusHoldingRegisters registers =new ModbusHoldingRegisters((int)sizeof/16);
        registers.setImpl(offset++,Hi);
        registers.setImpl(offset++,Low);
        registers.setImpl(offset++,HiHi);
        registers.setImpl(offset++,LowLow);
        double tempValue= registers.getFloat64At(0);
        offset=0;
        for (int r : registers){
            errMessage+=String.format("Addr:%d, Value: %d\n",offset++,r);
        }
        errMessage+=String.format("Float64: %4.15f",tempValue);
        log.debug(errMessage);
        errMessage=String.format("Float64: %4.15f",tempValue);
        return errMessage;
    }

    private String setValue(double value, int sizeof) throws IllegalDataValueException, IllegalDataAddressException {
        int offset=0;
        String errMessage=new String();
        ModbusHoldingRegisters registers =new ModbusHoldingRegisters((int)sizeof/16);
        registers.setFloat64At(0, value);
        double tempValue= registers.getFloat64At(0);
        for (int r : registers){
            errMessage+=String.format("Addr:%d, Value: %d\n",offset++,r);
        }
        errMessage+=String.format("Float64: %4.15f",tempValue);
        log.debug(errMessage);
        errMessage=String.format("Float64: %4.15f",tempValue);
        return errMessage;
    }
    private String setValue(float value, int sizeof) throws IllegalDataValueException, IllegalDataAddressException {
        int offset=0;
        String errMessage=new String();
        ModbusHoldingRegisters registers =new ModbusHoldingRegisters((int)sizeof/16);
        registers.setFloat32At(0, value);
        float tempValue= registers.getFloat32At(0);
        for (int r : registers){
            errMessage+=String.format("Addr:%d, Value: %d\n",offset++,r);
        }
        errMessage+=String.format("Float32: %4.15f",tempValue);
        log.debug(errMessage);
        errMessage=String.format("Float32: %4.15f",tempValue);
        return errMessage;
    }

    @Test
    public void testCodeDecode() throws IllegalDataValueException, IllegalDataAddressException {
//        log.info(setValue(Math.PI,Double.SIZE));
//        log.info(setValue((float) Math.PI,Float.SIZE));
//        log.info(setValue((double) 1/3,Double.SIZE));
//        log.info(setValue((float) 1/3,Float.SIZE));
        log.info(setValue((double) 2.712,Double.SIZE));
        log.info(setValue((float) 1.26400005817413330078,Float.SIZE));

    }
    @Test
    public void testSetHolder() throws IllegalDataValueException, IllegalDataAddressException {
//        log.info(setHolder32(51905, 16289,Float.SIZE));
        log.info(setHolder32(18956, 33343,Float.SIZE));
        log.info(setHolder32(33343, 18956,Float.SIZE));
//        log.info(setHolder32(26769, 27966,Float.SIZE));

//        log.info(setHolder64( 6148, 22030, 11698, 1344,Double.SIZE));
//        log.info(setHolder64(44060, 23108, 36672, 17547,Double.SIZE));

    }

}
