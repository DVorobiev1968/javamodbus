package ru.DVorobiev;

import jlibmodbus.Modbus;
import jlibmodbus.data.ModbusHoldingRegisters;
import jlibmodbus.data.mei.ReadDeviceIdentificationInterface;
import jlibmodbus.exception.ModbusIOException;
import jlibmodbus.exception.ModbusNumberException;
import jlibmodbus.exception.ModbusProtocolException;
import jlibmodbus.master.ModbusMaster;
import jlibmodbus.master.ModbusMasterFactory;
import jlibmodbus.master.ModbusMasterTCP;
import jlibmodbus.msg.base.mei.MEIReadDeviceIdentification;
import jlibmodbus.msg.base.mei.ReadDeviceIdentificationCode;
import jlibmodbus.msg.request.ReadHoldingRegistersRequest;
import jlibmodbus.msg.response.ReadHoldingRegistersResponse;
import jlibmodbus.slave.ModbusSlave;
import jlibmodbus.slave.ModbusSlaveFactory;
import jlibmodbus.tcp.TcpParameters;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import ru.DVorobiev.DataHolders.ModbusEventListener;
import ru.DVorobiev.DataHolders.UserDataHolder;
import ru.DVorobiev.report.DataRegister;
import ru.DVorobiev.report.ReportExcel;

import java.net.InetAddress;
import java.nio.charset.Charset;

@Slf4j
public class SlaveTest {
    private void runSlave(int idNodeSlave){
        try {
            ModbusSlave slave;
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            slave = ModbusSlaveFactory.createModbusSlaveTCP(tcpParameters);
            slave.setBroadcastEnabled(true);
            slave.setReadTimeout(1000);

            Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG);
//            Modbus.setLogLevel(Modbus.LogLevel.LEVEL_WARNINGS);
//            Modbus.setLogLevel(Modbus.LogLevel.LEVEL_RELEASE);
            UserDataHolder dh=new UserDataHolder(idNodeSlave);
            // Запишем в Slave узел свое адресное пространство
            slave.setDataHolder(dh);
            dh.addEventListener(new ModbusEventListener() {
                @Override
                public void onWriteToSingleCoil(int address, boolean value) {
                    log.debug(String.format("onWriteToSingleCoil: address: %d, value:%d",address,value));
                }

                @Override
                public void onWriteToMultipleCoils(int address, int quantity, boolean[] values) {
                    log.debug(String.format("onWriteToMultipleCoils: address: %d, quantity:%d",address,quantity));
                }

                @Override
                public void onWriteToSingleHoldingRegister(int address, int value) {
                    log.debug(String.format("onWriteToSingleHoldingRegister: address: %d, value:%d",address,value));
                }

                @Override
                public void onWriteToMultipleHoldingRegisters(int address, int quantity, int[] values) {
                    log.debug(String.format("onWriteToMultipleHoldingRegisters: address: %d, quantity:%d",address,quantity));
                }
            });
            // Установим адрес Slave-узла
            slave.setServerAddress(idNodeSlave);
            /*
             * using master-branch it should be #slave.open();
             */
            slave.listen();
            /*
             * since 1.2.8
             */
            if (slave.isListening()) {
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        synchronized (slave) {
                            slave.notifyAll();
                        }
                    }
                });
                synchronized (slave) {
                    slave.wait();
                }
                /*
                 * using master-branch it should be #slave.close();
                 */
                slave.shutdown();
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void runSlaveTest(int idNodeSlave){
        long start = System.currentTimeMillis();
        log.info(String.format("Starting runSlaveTest..."));
        runSlave(idNodeSlave);
        long time = System.currentTimeMillis() - start;
        log.info( String.format("RunSlaveTest complete time: %d(ms)", time));
    }

    @Ignore
    @Test
    public void SlaveTest(){
        runSlaveTest(0);
    }

    private float calc(float value){
        if (value==0)
            return (float) -9.99;
        else
            return 0-value;
    }
    private double calc(double value){
        if (value==0)
            return (double) -9.99;
        else
            return 0-value;
    }
    /** Метод читает/записывает значение в Slave-узел
     * @param idNodeSlave : идентификатор Slave узла
     * @param address : адрес регистра
     * @param key : признак чтение/запись
     * @param size : размерность
     * */
    public void runReadWrite(int idNodeSlave, int address, int key, int size){
        try {
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            ModbusMaster master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
            try {
                request.setServerAddress(idNodeSlave);
                request.setStartAddress(address);
                request.setQuantity(size);
                request.setTransactionId(1);
                try {
                    if (!master.isConnected()) {
                        master.connect();
                    }
                    ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.processRequest(request);
                    ModbusHoldingRegisters registers = response.getHoldingRegisters();
                    if (size==TypesData.SIZE_32) {
                        float float32At = registers.getFloat32At(0);
                        if (key==TypesData.READ_WRITE) {
                            ModbusHoldingRegisters outRegister = new ModbusHoldingRegisters(TypesData.SIZE_32);
                            float32At = calc(float32At);
                            outRegister.setFloat32At(0, float32At);
                            master.writeMultipleRegisters(idNodeSlave, address, outRegister.getRegisters());
                            outRegister.setFloat32At(0, (float) -9.999);
                            master.writeMultipleRegisters(idNodeSlave, address+1*TypesData.SIZE_32, outRegister.getRegisters());
                            outRegister.setFloat32At(0, (float) 9.999);
                            master.writeMultipleRegisters(idNodeSlave, address+2*TypesData.SIZE_32, outRegister.getRegisters());
                        }
                        log.debug(String.format("registers.getFloat32At: %4.10f\n",float32At));
                    }
                    if (size==TypesData.SIZE_WORD) {
                        double float64At = registers.getFloat64At(0);
                        if (key==TypesData.READ_WRITE) {
                            ModbusHoldingRegisters outRegister = new ModbusHoldingRegisters(TypesData.SIZE_WORD);
                            float64At = calc(float64At);
                            outRegister.setFloat64At(0, float64At);
                            master.writeMultipleRegisters(idNodeSlave, address, outRegister.getRegisters());
                        }
                        log.debug(String.format("registers.getFloat64At: %4.10f\n",float64At));
                    }
                } catch (ModbusProtocolException e) {
                    e.printStackTrace();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        master.disconnect();
                    } catch (ModbusIOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (ModbusNumberException e) {
                e.printStackTrace();
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void runMasterTest(int idNodeSlave, int key){
        try {
            ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d",idNodeSlave));
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            ModbusMaster master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
            request.setServerAddress(idNodeSlave);
            for (int i=0; i<TypesData.SIZE_WORD*TypesData.MAX_COUNT_REGISTER;i+=TypesData.SIZE_WORD) {
                long currentTimeMillis = System.currentTimeMillis();
                request.setStartAddress(i);
                request.setQuantity(TypesData.SIZE_WORD);
                request.setTransactionId(i);
                try {
                    if (!master.isConnected()) {
                        master.connect();
                    }
                    ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.processRequest(request);
                    ModbusHoldingRegisters registers = response.getHoldingRegisters();
                    double float64At=registers.getFloat64At(0);
                    if (key==TypesData.READ_WRITE){
                        ModbusHoldingRegisters outRegister = new ModbusHoldingRegisters(TypesData.SIZE_WORD);
                        float64At=calc(float64At);
                        outRegister.setFloat64At(0,float64At);
                        master.writeMultipleRegisters(idNodeSlave,i,outRegister.getRegisters());
                    }
                    DataRegister dataRegister=new DataRegister(i,currentTimeMillis,float64At);
                    reportExcel.list.add(dataRegister);
                    log.debug(String.format("registers.getFloat64At: %d;%4.10f\n",i,float64At));
                } catch (ModbusProtocolException e) {
                    e.printStackTrace();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        master.disconnect();
                    } catch (ModbusIOException e) {
                        e.printStackTrace();
                    }
                }
            }
            reportExcel.CreateReport();

        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /** Метод производит чтение содержимого указанного диапазона регистров, в указанном Slave-узле
     * @param idNodeSlave : идентификатор узла
     * @param startAddress: стартовый адрес регистра
     * @param count : кол-во регистров в диапазоне
     * @param size : размерность слова с данными */
    public void runMasterTest(int idNodeSlave, int startAddress, int count, int size){
        try {
            ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d",idNodeSlave));
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            ModbusMaster m = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
            request.setServerAddress(idNodeSlave);
            try {
                if (!m.isConnected()) {
                    m.connect();
                }
                for (int i=startAddress; i<size*count;i+=size) {
                    long currentTimeMillis = System.currentTimeMillis();
                    request.setStartAddress(i);
                    request.setQuantity(size);
                    request.setTransactionId(i);
                    ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) m.processRequest(request);
                    ModbusHoldingRegisters registers = response.getHoldingRegisters();
                    if (size==TypesData.SIZE_WORD) {
                        double float64At = registers.getFloat64At(0);
                        DataRegister dataRegister = new DataRegister(i, currentTimeMillis, float64At);
                        reportExcel.list.add(dataRegister);
                        log.debug(String.format("registers.getFloat64At: %d;%4.10f\n", i, float64At));
                    }
                    if (size==TypesData.SIZE_32) {
                        float float32At = registers.getFloat32At(0);
                        DataRegister dataRegister = new DataRegister(i, currentTimeMillis,(double) float32At);
                        reportExcel.list.add(dataRegister);
                        log.debug(String.format("registers.getFloat32At: %d;%4.10f\n", i, float32At));
                    }
                }
                } catch (ModbusProtocolException e) {
                    e.printStackTrace();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        m.disconnect();
                    } catch (ModbusIOException e) {
                        e.printStackTrace();
                    }
                }
            reportExcel.CreateReport();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readInfoSlaveNode(ModbusMaster master, int idSlave, int Addr) throws ModbusProtocolException, ModbusNumberException, ModbusIOException {
        String errMessage;
        errMessage=String.format("");
        MEIReadDeviceIdentification rdi = master.readDeviceIdentification(idSlave, Addr, ReadDeviceIdentificationCode.BASIC_STREAM_ACCESS);
        ReadDeviceIdentificationInterface.DataObject[] objects = rdi.getObjects();
        errMessage+=String.format("%s\t", "Device identification");
        for (ReadDeviceIdentificationInterface.DataObject o : objects) {
            errMessage+=String.format("%s ", new String(o.getValue(), Charset.defaultCharset()));
        }
        return errMessage;
    }
    /** Вывод информации по значения определенного регистра Slave-узла
     * @param idNodeSlave : идентификатор узла
     * @param startAddress : стартовый адрес
     * @param quantity : длина слова */
    public void runMasterTest(int idNodeSlave, int startAddress, int quantity){
        try {
            Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG);
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            ModbusMaster master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            try {
                if (!master.isConnected()) {
                    master.connect();
                }
                // получим информацию о Slave-узле
//                String infoSlave=readInfoSlaveNode(master,idNodeSlave,startAddress);
//                log.info(infoSlave);
                // Получим информацию по аналоговому выходу
                ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
                request.setServerAddress(idNodeSlave);
                request.setStartAddress(startAddress);
                request.setQuantity(quantity);
                request.setTransactionId(0);
                ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.processRequest(request);
                ModbusHoldingRegisters registers = response.getHoldingRegisters();
                for (int r : registers) {
                    log.debug(String.format("[%d]",r));
                }
                if (quantity==Float.BYTES/2){
                    float float32At=registers.getFloat32At(0);
                    log.info(String.format("registers.getFloat32At: %4.10f\n",float32At));
                }
                if (quantity==Double.BYTES/2){
                    double float64At=registers.getFloat64At(0);
                    log.info(String.format("registers.getFloat64At: %4.10f\n",float64At));
                }

            } catch (ModbusProtocolException e) {
                e.printStackTrace();
            } catch (ModbusIOException e) {
                e.printStackTrace();
            } finally {
                try {
                    master.disconnect();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                }
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Ignore
    @Test
    public void MasterTest(){
//        runMasterTest(1,1,2);
//        runMasterTest(2,1,4);
        runMasterTest(1,1,TypesData.MAX_COUNT_REGISTER,TypesData.SIZE_32);
    }

    @Ignore
    @Test
    public void MasterTestExt(){
        runReadWrite(1,1,TypesData.READ_WRITE,TypesData.SIZE_32);
    }
    @Ignore
    @Test
    public void runMultiSlave(){
        try {
            ThreadSlave threadSlave=new ThreadSlave("Slave 1");
            threadSlave.setIdNodeSlave(1);
            threadSlave.start();
            ThreadMaster threadMaster=new ThreadMaster("Master 1");
            threadMaster.setIdNodeSlave(1);
            threadMaster.setStartAddres(0);
            threadMaster.setQuantity(2);
            threadMaster.start();
            while (threadMaster.getStateThread()<ThreadMaster.CANCEL_THREAD)
                Thread.sleep(1000);
            threadSlave.threadTest.join(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
