package examples;

import jlibmodbus.Modbus;
import jlibmodbus.exception.ModbusIOException;
import jlibmodbus.exception.ModbusNumberException;
import jlibmodbus.exception.ModbusProtocolException;
import jlibmodbus.master.ModbusMaster;
import jlibmodbus.master.ModbusMasterFactory;
import jlibmodbus.msg.request.ReadHoldingRegistersRequest;
import jlibmodbus.msg.response.ReadHoldingRegistersResponse;
import jlibmodbus.tcp.TcpParameters;

import java.net.InetAddress;

public class Client {

    static public void main(String[] args) {
        try {
            TcpParameters tcpParameters = new TcpParameters();
            InetAddress host=InetAddress.getByName("localhost");

            //tcp parameters have already set by default as in example
            // tcpParameters.setHost(InetAddress.getLocalHost());
            tcpParameters.setHost(host);
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);

            //if you would like to set connection parameters separately,
            // you should use another method: createModbusMasterTCP(String host, int port, boolean keepAlive);
            ModbusMaster m = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);

            int slaveId = 0x0;
            int offset = 12;
            int quantity =2;

            try {
                // since 1.2.8
                if (!m.isConnected()) {
                    m.connect();
                }
                // also since 1.2.8.4 you can create your own request and process it with the master
                ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
                request.setServerAddress(slaveId);
                request.setStartAddress(offset);
                request.setTransactionId(0);
                request.setQuantity(quantity);
                ReadHoldingRegistersResponse response = new ReadHoldingRegistersResponse();
                response = (ReadHoldingRegistersResponse) m.processRequest(request);
                //you can get either int[] containing register values or byte[] containing raw bytes.
                for (int value : response.getRegisters()) {
                    System.out.println("Address: " + offset++ + ", Value: " + value);
                }
            } catch (ModbusProtocolException e) {
                e.printStackTrace();
            } catch (ModbusNumberException e) {
                e.printStackTrace();
            } catch (ModbusIOException e) {
                e.printStackTrace();
            } finally {
                try {
                    m.disconnect();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                }
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
