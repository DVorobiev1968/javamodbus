package examples;

import jlibmodbus.Modbus;
import jlibmodbus.data.ModbusHoldingRegisters;
import jlibmodbus.exception.ModbusIOException;
import jlibmodbus.exception.ModbusProtocolException;
import jlibmodbus.master.ModbusMaster;
import jlibmodbus.master.ModbusMasterFactory;
import jlibmodbus.msg.request.ReadHoldingRegistersRequest;
import jlibmodbus.msg.response.ReadHoldingRegistersResponse;
import jlibmodbus.tcp.TcpParameters;

import java.net.InetAddress;

public class MasterTCP {

    static public void main(String[] args) {
        try {
            TcpParameters tcpParameters = new TcpParameters();
            tcpParameters.setHost(InetAddress.getByName("localhost"));
            tcpParameters.setKeepAlive(true);
            tcpParameters.setPort(Modbus.TCP_PORT);
            ModbusMaster m = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest();
            int slaveId = 0xff;
            int offset = 12;
            int quantity = 2;

            request.setServerAddress(slaveId);
            request.setStartAddress(offset);
            request.setQuantity(quantity);
            request.setTransactionId(0);

            try {
                if (!m.isConnected()) {
                    m.connect();
                }
                ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) m.processRequest(request);
                ModbusHoldingRegisters registers = response.getHoldingRegisters();
                for (int r : registers) {
                    System.out.println(r);
                }
//                System.out.println("registers.getFloat64At: " + registers.getFloat64At(0));
                float realValue=registers.getFloat32At(0);
                System.out.format("registers.getFloat32At: %4.10f\n",realValue);
//                System.out.println("registers.getInt64At: " + registers.getInt64At(0));
                System.out.println("registers.getInt32At: " + registers.getInt32At(0));
                System.out.println("registers.getInt16At: " + registers.getInt16At(0));

            } catch (ModbusProtocolException e) {
                e.printStackTrace();
            } catch (ModbusIOException e) {
                e.printStackTrace();
            } finally {
                try {
                    m.disconnect();
                } catch (ModbusIOException e) {
                    e.printStackTrace();
                }
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
