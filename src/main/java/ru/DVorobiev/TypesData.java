package ru.DVorobiev;

import lombok.experimental.UtilityClass;

/** Класс объявления типов данных значений узлов, применяемый на Сервере технологических данных */
@UtilityClass
public class TypesData {
    /** по умолчанию тип данных double 4 Регистра */
    public static final int SIZE_WORD = 4;
    /**  размерность для 32 битного слова */
    public static final int SIZE_32 = 2;
    /** максимальное кол-во возможных значений*/
    public static final int MAX_COUNT_REGISTER=250;
    /** признак READ_WRITE применяется для тестирования */
    public static final int READ_WRITE=-1;
    /** признак READ применяется для тестирования, только читает данные из регистра */
    public static final int READ=0;
    /** признак WRITE применяется для тестирования, только записывает данные в регистр */
    public static final int WRITE=1;

}
