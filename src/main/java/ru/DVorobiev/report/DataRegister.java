package ru.DVorobiev.report;

import lombok.Data;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Data
public class DataRegister {
    /** Метка времени дата */
    private Date date;
    /** Метка времени */
    private long time;
    /** Адрес регистра */
    private int addr;
    /** Значение регистра */
    private Double value;

    /**
     * Дату присваеваем автоматически
     *
     * @param addr: идентификатор узла
     * @param time : метка времени
     * @param value: значение от процесса
     */
    public DataRegister(int addr, long time, Double value) {
        this.addr = addr;
        this.date = new Date();
        this.time = time;
        this.value = value;
    }

    /**
     * метод который работает по умолчанию, автоматически присваивает текущую дату
     *
     * @return String: строка дата время с учетом Locale
     */
    public String getDateString() {
        String dateStr;
        Locale locale = new Locale("ru", "RU");
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setWeekdays(
                new String[] {
                    "Не используется",
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });

        String pattern = "dd/MM/yyyy HH:mm:ss.SSS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    /**
     * метод который преобразовывает дату и время с учетом Locale
     *
     * @param date дата
     * @return String: строка дата время с учетом Locale
     */
    public String getDateString(Date date) {
        String dateStr, timeStr;
        String pattern;

        SimpleDateFormat simpleDateFormat;
        Locale locale = new Locale("ru", "RU");
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setWeekdays(
                new String[] {
                    "Не используется",
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });

        pattern = "dd/MM/yyyy";
        simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        this.setDate(new Date(dateStr));

        pattern = "dd/MM/yyyy HH:mm:ss.SSS";
        simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        return dateStr;
    }
}
