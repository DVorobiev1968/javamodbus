package ru.DVorobiev.report;

import lombok.Getter;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import ru.DVorobiev.ErrorCode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/** класс для вывода информации в Excel */
@Getter
public class ReportExcel {
    /** объект рабочей книги */
    public HSSFWorkbook workbook;

    public HSSFSheet sheet;
    public List<DataRegister> list;
    /** Сообщение об ошибке */
    private String errMessage;
    /** Путь с именем файла данных по умолчанию ./(name_sheet).xls */
    public String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Конструктор для вывода Объекта DataRegisterDAO.dataRegisterList() в лист рабочей книги
     *
     * @param name_sheet : имя листа рабочей книги
     */
    public ReportExcel(String name_sheet) {
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet(name_sheet);
        this.list = dataRegisterDAO.dataRegisterList();
        path = String.format("%s.xls", name_sheet);
    }

    /**
     * Метод формирует отчет в Excel
     *
     * @return : ErrorCode.XLS_OK, ErrorCode.XLS_ERR - в случае исключения, ErrorCode.XLS_EMPTY -
     *     нет данных
     */
    public int CreateReport() {
        int rownum = 0;
        Cell cell;
        Row row;

        HSSFCellStyle style = createStyleForTitle(workbook);
        row = sheet.createRow(rownum);
        // Date
        cell = row.createCell(0, CellType.STRING);
        cell.setCellValue("DataTime");
        cell.setCellStyle(style);
        // miliseconds
        cell = row.createCell(1, CellType.STRING);
        cell.setCellValue("ms");
        cell.setCellStyle(style);
        // Addr
        cell = row.createCell(2, CellType.STRING);
        cell.setCellValue("Addr");
        cell.setCellStyle(style);
        // Value
        cell = row.createCell(3, CellType.STRING);
        cell.setCellValue("value");
        cell.setCellStyle(style);

        // Data
        for (DataRegister item : list) {
            rownum++;
            row = sheet.createRow(rownum);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(item.getDateString(item.getDate()));
            cell = row.createCell(1, CellType.NUMERIC);
            cell.setCellValue(item.getTime());
            cell = row.createCell(2, CellType.NUMERIC);
            cell.setCellValue(item.getAddr());
            cell = row.createCell(3, CellType.NUMERIC);
            cell.setCellValue(item.getValue());
        }
        if (rownum > 0) {
            try {
                File file = new File(String.format("reports/%s", path));
                file.getParentFile().mkdir();

                FileOutputStream outFile = new FileOutputStream(file);
                workbook.write(outFile);
                errMessage = String.format("Created file: %s", file.getAbsolutePath());
                return ErrorCode.XLS_OK;
            } catch (IOException e) {
                errMessage = e.getMessage();
                return ErrorCode.XLS_ERR;
            }
        }
        errMessage = String.format("Count rows: %d. Report not create.", rownum);
        return ErrorCode.XLS_EMPTY;
    }

    /**
     * Задаем необходимый стиль для работы в документе
     *
     * @param workbook: путь и имя файла с выгрузкой данных
     * @return style: возвращаем объект стиля документа
     */
    private HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFont(font);
        return style;
    }
}
