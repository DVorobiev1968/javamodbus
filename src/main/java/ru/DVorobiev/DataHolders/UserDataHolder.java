package ru.DVorobiev.DataHolders;

import jlibmodbus.data.DataHolder;
import jlibmodbus.data.ModbusHoldingRegisters;
import jlibmodbus.exception.IllegalDataAddressException;
import jlibmodbus.exception.IllegalDataValueException;
import ru.DVorobiev.TypesData;
import ru.DVorobiev.report.DataRegister;
import ru.DVorobiev.report.ReportExcel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserDataHolder extends DataHolder {

    final List<ModbusEventListener> modbusEventListenerList = new ArrayList<ModbusEventListener>();
    /** Создаем свою структуру хранилища данных с по регистрам */
    public UserDataHolder(int idSlave) throws IllegalDataValueException, IllegalDataAddressException {

        int sizeWordToRegistr= TypesData.SIZE_WORD;
        int modbusHRCount=TypesData.SIZE_WORD*TypesData.MAX_COUNT_REGISTER;
        ReportExcel reportExcel = new ReportExcel(String.format("Slave_%d_HR",idSlave));

        Random random=new Random();
        ModbusHoldingRegisters hr = new ModbusHoldingRegisters(modbusHRCount);
        for (int i=0; i<modbusHRCount;i+=TypesData.SIZE_WORD){
            long currentTimeMillis = System.currentTimeMillis();
            Double dValueRandom=random.nextDouble();
            DataRegister dataRegister=new DataRegister(i,currentTimeMillis,dValueRandom);
            reportExcel.list.add(dataRegister);
            hr.setFloat64At(i,dValueRandom);
        }
        setHoldingRegisters(hr);
        reportExcel.CreateReport();
    }

    public void addEventListener(ModbusEventListener listener) {
        modbusEventListenerList.add(listener);
    }

    public boolean removeEventListener(ModbusEventListener listener) {
        return modbusEventListenerList.remove(listener);
    }

    @Override
    public void writeHoldingRegister(int offset, int value) throws IllegalDataAddressException, IllegalDataValueException {
        for (ModbusEventListener l : modbusEventListenerList) {
            l.onWriteToSingleHoldingRegister(offset, value);
        }
        super.writeHoldingRegister(offset, value);
    }

    @Override
    public void writeHoldingRegisterRange(int offset, int[] range) throws IllegalDataAddressException, IllegalDataValueException {
        for (ModbusEventListener l : modbusEventListenerList) {
            l.onWriteToMultipleHoldingRegisters(offset, range.length, range);
        }
        super.writeHoldingRegisterRange(offset, range);
    }

    @Override
    public void writeCoil(int offset, boolean value) throws IllegalDataAddressException, IllegalDataValueException {
        for (ModbusEventListener l : modbusEventListenerList) {
            l.onWriteToSingleCoil(offset, value);
        }
        super.writeCoil(offset, value);
    }

    @Override
    public void writeCoilRange(int offset, boolean[] range) throws IllegalDataAddressException, IllegalDataValueException {
        for (ModbusEventListener l : modbusEventListenerList) {
            l.onWriteToMultipleCoils(offset, range.length, range);
        }
        super.writeCoilRange(offset, range);
    }
}
