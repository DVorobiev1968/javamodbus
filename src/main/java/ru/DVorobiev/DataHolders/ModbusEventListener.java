package ru.DVorobiev.DataHolders;

public interface ModbusEventListener {
    /** Метод для записи Битового значения в регистр*/
    void onWriteToSingleCoil(int address, boolean value);

    /** Метод для записи Битовых значений в регистр */
    void onWriteToMultipleCoils(int address, int quantity, boolean[] values);

    /** Метод для записи значения типа int в регистр */
    void onWriteToSingleHoldingRegister(int address, int value);

    /** Метод для записи значения типа float(32bit), double(64bit) в регистры */
    void onWriteToMultipleHoldingRegisters(int address, int quantity, int[] values);
}
