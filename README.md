Описание проекта JavaModbus
======================================
Maven проект JavaModbus построен на проекте:
The MODBUS protocol implementation in pure java.

The main advantages of the library are following:
- the library supports a lot of communication libraries (jssc, rxtx, purejavacomm, java comm api);
- the library has (practically) complete implementation of the modbus protocol v1.1b.

AUTHORS

Vladislav Y. Kochedykov, software engineer, technical expert.

CONTACT

If you have problems, questions or suggestions, please contact me at email address vladislav.kochedykov@gmail.com

To learn how to use the library you can either use examples in "examples\com\invertor\examples\modbus" folder or contact me at email.

Maven dependency:

The latest stable release.
<dependency>
<groupId>com.intelligt.modbus</groupId>
<artifactId>jlibmodbus</artifactId>
<version>1.2.9.7</version>
</dependency>

WEB SITE

project homepage:
http://jlibmodbus.sourceforge.net
https://github.com/kochedykov/jlibmodbus

purejavacomm homepage:
http://www.sparetimelabs.com/purejavacomm/purejavacomm.php
https://github.com/nyholku/purejavacomm

jssc home page:
https://code.google.com/p/java-simple-serial-connector
https://github.com/scream3r/java-simple-serial-connector

rxtx home page:
http://users.frii.com/jarvi/rxtx/index.html

jserialcomm homepage:
https://github.com/Fazecast/jSerialComm

Проект находится <git@gitlab.com:DVorobiev1968/javamodbus.git>
Установка  JavaModbus

1. mkdir <targetDir>
2. cd <targetDir>
3. git clone git@gitlab.com:DVorobiev1968/javamodbus.git
4. если проект уже ранее был клонирован, то обновляемся до последней версии:
  1. git pull
5. mvn clean install.

Содержание проекта
---------------------

Документация находится в стадии разработки